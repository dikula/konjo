package com.example.dikula.konjo;


public class ButtonCordinates {
    int x;
    int y;
    int id;


    public void setXb(int x) {
        this.x = x;
    }

    public void setYb(int y) {
        this.y = y;
    }

    public int getXb() {
        return x;
    }

    public int getYb() {
        return y;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int[] getid() {
        int[] koridnate = new int[]{x, y};
        return koridnate;
    }

    public ButtonCordinates(int x, int y, int id){
        this.x = x;
        this.y = y;
        this.id = id;
    }


}
