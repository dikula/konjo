package com.example.dikula.konjo;


import android.util.Log;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.URL;
import java.net.URLConnection;


public class ConnectionToServer {

    public static String get_response_from_url(String url) {
        String contents ="";

        try {
            URLConnection conn = new URL(url).openConnection();
            InputStream in = conn.getInputStream();
            Log.i("INININININININ", String.valueOf(in));
            contents = convert_stream_to_string(in);
            Log.i("ONLINE MOVES1", String.valueOf(contents));
        }catch(IOException e){
            Log.i("ONLINE MOVES12", String.valueOf(url));
            Log.i("URL URL URL", String.valueOf(url));
        } catch (Exception e){
            Log.i("ONLINE MOVES123", String.valueOf(url));
            e.printStackTrace();
            return null;
        }

        return contents;
    }

    private static String convert_stream_to_string(InputStream is) throws UnsupportedEncodingException {

        BufferedReader reader = new BufferedReader(new
                InputStreamReader(is, "UTF-8"));
        StringBuilder sb = new StringBuilder();
        String line = null;
        try {
            while ((line = reader.readLine()) != null) {
                sb.append(line + "");
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                is.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return sb.toString();
    }

    public static String add_highscore(String name, Integer moves, Integer time) {
        String url = String.format("http://198.211.126.94/konjo/add_record.php?new_record&name=%s&moves=%d&time=%d", name, moves, time);
        String contents ="";

        Log.i("URL URL URL", String.valueOf(url));
        try {
            URLConnection conn = new URL(url).openConnection();
            InputStream in = conn.getInputStream();
            contents = convert_stream_to_string(in);
        }catch(IOException e){
            Log.i("URL URL URL", String.valueOf(url));
        } catch (Exception e){
            e.printStackTrace();
            return null;
        }
        return contents;
    }
}
