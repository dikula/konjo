package com.example.dikula.konjo;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.StrictMode;
import android.util.Log;
import android.widget.ListView;
import android.widget.SimpleAdapter;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;


public class Highscore extends Activity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.highscore);

        // Allowing connection to server
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);

        ListView lv = (ListView) findViewById(R.id.lista);
        ArrayList<HashMap<String, String>> feedList = new ArrayList<HashMap<String, String>>();
        try {
            feedList = show_results();
            SimpleAdapter simpleAdapter = new SimpleAdapter(this, feedList, R.layout.list_view,
                    new String[]{"name", "moves", "time"}, new int[]{R.id.textViewDate,
                    R.id.textViewDescription, R.id.textViewPrice});
            lv.setAdapter(simpleAdapter);
        } catch (JSONException e) {
            e.printStackTrace();

        }
        catch (Exception e){
            //Log.i("ARRAY123123", String.valueOf("results"));
            AlertDialog alertDialog = new AlertDialog.Builder(this).create();
            alertDialog.setTitle("High score");
            alertDialog.setMessage("Please connect to internet to get High scores");
            alertDialog.setCanceledOnTouchOutside(false);
            alertDialog.setButton(Dialog.BUTTON_POSITIVE, "OK", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    exit_game();
                }
            });

            alertDialog.show();
        }

    }


    private ArrayList<HashMap<String, String>> show_results() throws JSONException {
        JSONArray results;

        results =  get_results();
        Log.i("ARRAY", String.valueOf(results));


        ArrayList<HashMap<String, String>> feedList= new ArrayList<HashMap<String, String>>();
        HashMap<String, String> map = new HashMap<String, String>();
        map.put("name", "Name");
        map.put("moves", "Moves");
        map.put("time", "Time");
        feedList.add(map);


        for(int i=0; i<results.length(); i++){

            JSONObject jsonProductObject = results.getJSONObject(i);
            String name = jsonProductObject.getString("name");
            //Log.i("ARRAY NAME", String.valueOf(name));

            String moves = jsonProductObject.getString("moves");
            String time = jsonProductObject.getString("time");
            String date = jsonProductObject.getString("date_entry");

            map = new HashMap<String, String>();
            map.put("name", name);
            map.put("moves", moves);
            map.put("time", time);
            feedList.add(map);

        }
        Log.i("FEED LIST", String.valueOf(feedList));
        return feedList;

    }

    private JSONArray get_results() throws JSONException{
        String url = "http://198.211.126.94/konjo/get_record.php";
        String get_results = ConnectionToServer.get_response_from_url(url);

        //get_results = get_results.substring(0, get_results.length() - 1);
        Log.i("results123", String.valueOf(get_results));


        JSONArray jObj = null;// = new JSONObject;

        //JSONObject jsonObject = new JSONObject(get_results);

        jObj = new JSONArray(get_results);
        Log.i("results123", String.valueOf(jObj));
//        try {
//            jObj = new JSONArray(get_results);
//            Log.i("123321123321", String.valueOf(jObj));
//        } catch (JSONException e) {
//            Log.e("JSON Parser", "Error parsing data " + e.toString());
//        }
        return jObj;
    }

    private void exit_game() {
        Intent intent = new Intent(this, MyActivity.class);
        startActivity(intent);
        finish();
    }
}
