package com.example.dikula.konjo;

import android.app.Activity;
import android.app.Application;
import android.content.Context;
import android.util.Log;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.OutputStreamWriter;

public class LocalStorage extends Activity{
    public static String FILENAME = "my_high_score";
    //public static String FILENAME = "draw_board";

    public static void save_highscore_local(String highscore,  Context ctx){
        FileOutputStream fos=null;
        try {
            fos =  ctx.openFileOutput(FILENAME, Context.MODE_PRIVATE);
            fos.write(highscore.getBytes());
            fos.close();
        } catch (FileNotFoundException e){
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                fos.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        //return "OK";
    }

    public static String get_highscore_local(Context ctx){
        Log.i("HIGHSCORE", "123123");
        FileInputStream file=null;
        try {
            Log.i("HIGHSCORE", "123123");
            file = ctx.openFileInput(FILENAME);
            byte[] buffer = new byte[(int) file.getChannel().size()];
            file.read(buffer);
            String str= "";
            for(byte b:buffer) str+=(char)b;
            file.close();

            return str;

        } catch (FileNotFoundException e) {
            Log.i("HIGHSCORE", String.valueOf("123123"));
        } catch (Exception ex){
            Log.i("HIGHSCORE123", String.valueOf("123123"));
        }
        if (file != null) {
            Log.i("123321123", file.toString());
            return file.toString();
        }
        return null;
    }
}
