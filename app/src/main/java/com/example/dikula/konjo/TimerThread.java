package com.example.dikula.konjo;

import android.app.Activity;

public class TimerThread implements Runnable {
    private boolean run = false;
    private static final int STEP = 100;
    private boolean paused = false;
    private Object mutex = new Object();
    private Activity activity;

    public TimerThread(Activity activity) {
        this.activity = activity;
    }

    public void setRunning(boolean run) {
        this.run = run;
    }

    public void suspend(){
        paused = true;
    }

    public void resume(){
        paused = false;
        synchronized (mutex) {
            mutex.notifyAll();
        }
    }

    @Override
    public void run() {
        while (run) {
            synchronized (mutex) {
                while (paused)
                    try {
                        mutex.wait();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
            }
            try {
                Thread.sleep(STEP);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
//if (timerThreadHolder == null) {
//        timer = new TimerThread(this);
//        timerThreadHolder = new Thread(timer);
//        handler.post(new Runnable() {
//          @Override
//          public void run() {
//              timer.setRunning(true);
//              timerThreadHolder.start();
//          }
//        });
//        showQuestion();
//        } else {
//          timer.resume();
//          showQuestion();
//        }
//  }